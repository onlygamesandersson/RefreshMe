package com.hpscds.refreshme;


import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.hpscds.refreshme.colors.Colors;

public class ColorsActivity extends AndroidApplication {
	//ESTE ES LA CLASE QUE INICIA EL CICLO DE VIDA DE LIBGDX
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize(new Colors());
	}
}
