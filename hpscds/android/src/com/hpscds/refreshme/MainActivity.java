package com.hpscds.refreshme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity  extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void MatchPairsActivity(View view) {
        startActivity(new Intent(this, MatchPairsActivity.class));
    }

    public void ColorsActivity(View view){

        startActivity(new Intent(this, ColorsActivity.class));
    }

    public void MathsActivity(View view){

        startActivity(new Intent(this, MathsActivity.class));
    }


}
