package com.hpscds.refreshme.colors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.hpscds.refreshme.Constants;
import com.hpscds.refreshme.MyAssetsManager;
import com.hpscds.refreshme.entities.Card;
import com.hpscds.refreshme.entities.CardCircle;
import com.hpscds.refreshme.entities.CardCube;
import com.hpscds.refreshme.entities.CardHexagon;
import com.hpscds.refreshme.entities.CardStar;
import com.hpscds.refreshme.entities.CardTriangle;
import com.hpscds.refreshme.screens.AbstractScreen;
import com.hpscds.refreshme.screens.GameOverScreen;
import java.util.ArrayList;

public class ColorsGameScreen extends AbstractScreen {

    public ColorsGameScreen(Colors colors) {
        super(colors);
    }

    /**
     * Class attributes
     */
    public Colors colors;
    public AbstractScreen gameOverScreen;
    private MyAssetsManager assetManager = new MyAssetsManager();
    private SpriteBatch batch;
    private OrthographicCamera camera;

    private Texture backgroundTexture;

    private Texture circleTexture;
    private Texture cubeTexture;
    private Texture hexagonTexture;
    private Texture starTexture;
    private Texture triangleTexture;
    private Texture scoreTexture;

    private CardCircle cardCircle;
    private CardCube cardCube;
    private CardHexagon cardHexagon;
    private CardStar cardStar;
    private CardTriangle cardTriangle;

    private ArrayList<Card> cards;
    private ArrayList<Integer> array_of_figures;
    private Vector2 clickPosition = new Vector2();
    private Vector3 cameraUnprojectCoordinates = new Vector3();

    private int touchCounter;
    private int figureIndex;
    private boolean fail;
    private float elapsedTime;

    private TextureAtlas charsetCircle;
    private TextureAtlas charsetCube;
    private TextureAtlas charsetHexagon;
    private TextureAtlas charsetStar;
    private TextureAtlas charsetTriangle;
    private TextureRegion currentFrame;

    private Animation circleAnimation;
    private Animation cubeAnimation;
    private Animation hexagonAnimation;
    private Animation starAnimation;
    private Animation triangleAnimation;


    private BitmapFont font;
    private int score;
    private String scoreString;

    private Sound circleSound, cubeSound, hexagonSound, starSound, triangleSound, failSound;
    private Music backgroundMusic;
    private boolean isPlayingSound;

    private ArrayList<Integer> points;
    private int correctAnswers;
    private int finalScore;

    @Override
    public void show() {
        colors = new Colors();
        assetManager.loadTextures();
        assetManager.loadAnimations();
        assetManager.loadSounds();
        assetManager.manager.finishLoading();

        batch = new SpriteBatch();
        camera = new OrthographicCamera(Constants.getViewportWidth(), Constants.getViewportHeight());
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        cardCircle = new CardCircle(Constants.getPosition0());
        cardCube = new CardCube(Constants.getPosition1());
        cardHexagon = new CardHexagon(Constants.getPosition2());
        cardStar = new CardStar(Constants.getPosition3());
        cardTriangle = new CardTriangle(Constants.getPosition5());

        backgroundTexture = assetManager.manager.get(Constants.getColorsBackgroundUrl());
        circleTexture = assetManager.manager.get(Constants.getCircleCardUrl());
        cubeTexture = assetManager.manager.get(Constants.getCubeCardUrl());
        hexagonTexture = assetManager.manager.get(Constants.getHexagonCardUrl());
        starTexture = assetManager.manager.get(Constants.getStarCardUrl());
        triangleTexture = assetManager.manager.get(Constants.getTraingleCardUrl());
        scoreTexture = assetManager.manager.get(Constants.getScoreUrl());

        charsetCircle = assetManager.manager.get(Constants.getCircleAnimationUrl());
        charsetCube = assetManager.manager.get(Constants.getCubeAnimationUrl());
        charsetHexagon = assetManager.manager.get(Constants.getHexagonAnimationUrl());
        charsetStar = assetManager.manager.get(Constants.getStarAnimationUrl());
        charsetTriangle = assetManager.manager.get(Constants.getTriangleAnimationUrl());

        circleSound = assetManager.manager.get(Constants.getCircleSoundUrl());
        cubeSound = assetManager.manager.get(Constants.getCubeSoundUrl());
        hexagonSound = assetManager.manager.get(Constants.getHexagonSoundUrl());
        starSound = assetManager.manager.get(Constants.getStarSoundUrl());
        triangleSound = assetManager.manager.get(Constants.getTriangleSoundUrl());
        failSound = assetManager.manager.get(Constants.getFailSoundUrl());
        backgroundMusic = assetManager.manager.get(Constants.getBackgroundMusicUrl());

        Array<TextureAtlas.AtlasRegion> circleFrames = charsetCircle.findRegions("circle");
        Array<TextureAtlas.AtlasRegion> cubeFrames = charsetCube.findRegions("cube");
        Array<TextureAtlas.AtlasRegion> hexagonFrames = charsetHexagon.findRegions("hexagon");
        Array<TextureAtlas.AtlasRegion> starFrames = charsetStar.findRegions("star");
        Array<TextureAtlas.AtlasRegion> triangleFrames = charsetTriangle.findRegions("triangle");

        circleAnimation = new Animation(Constants.getAnimationFrameDuration(), circleFrames, Animation.PlayMode.LOOP_PINGPONG); //0.075f
        cubeAnimation = new Animation(Constants.getAnimationFrameDuration(), cubeFrames, Animation.PlayMode.LOOP_PINGPONG);
        hexagonAnimation = new Animation(Constants.getAnimationFrameDuration(), hexagonFrames, Animation.PlayMode.LOOP_PINGPONG);
        starAnimation = new Animation(Constants.getAnimationFrameDuration(), starFrames, Animation.PlayMode.LOOP_PINGPONG);
        triangleAnimation = new Animation(Constants.getAnimationFrameDuration(), triangleFrames, Animation.PlayMode.LOOP_PINGPONG);

        cards = new ArrayList<Card>();
        cards.add(cardCircle);
        cards.add(cardCube);
        cards.add(cardHexagon);
        cards.add(cardStar);
        cards.add(cardTriangle);

        array_of_figures = new ArrayList<Integer>();
        elapsedTime = 0;
        figureIndex = 0;
        touchCounter = 0;
        fail = false;
        font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font.png"), false);
        score = 0;
        scoreString = "";
        backgroundMusic = assetManager.manager.get(Constants.getBackgroundMusicUrl());
        backgroundMusic.setVolume(1.0f);
        backgroundMusic.setLooping(true);
        backgroundMusic.play();
        isPlayingSound = false;

        correctAnswers = 0;
        finalScore = 0;
        points = new ArrayList<Integer>();
    }

    /**
     * Main loop where games begin, also is here where we paint on screen background, score texture and the font used to display the score.
     */
    @Override
    public void render(float delta) {
        elapsedTime += delta;
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(backgroundTexture, -(Constants.getViewportWidth() / 2), -(Constants.getViewportHeight() / 2), Constants.getViewportWidth(), Constants.getViewportHeight());
        batch.draw(scoreTexture, -(Constants.getViewportWidth() / 2), Constants.getViewportHeight() / 3, Constants.getViewportWidth(), Constants.getViewportHeight() / 8);
        game();
        scoreString = String.valueOf(score);
        font.getData().setScale(0.056f, 0.026f);
        font.setColor(Color.BLACK);
        font.draw(batch, scoreString, -8, 7, 10, 0, false);
        batch.end();
    }

    /**
     * Generate a random color between 5 posibilities.
     *
     * @return assigned id to current color
     */
    private int generateRandomColor() {
        return (int) (Math.random() * 5) + 1;
    }

    /**
     * Draw the static board when player must enter his answers.
     */
    private void drawCardsGrid() {

        batch.draw(circleTexture, cardCircle.getPosition().x, cardCircle.getPosition().y, cardCircle.getDimension().x, cardCircle.getDimension().y);
        batch.draw(cubeTexture, cardCube.getPosition().x, cardCube.getPosition().y, cardCube.getDimension().x, cardCube.getDimension().y);
        batch.draw(hexagonTexture, cardHexagon.getPosition().x, cardHexagon.getPosition().y, cardHexagon.getDimension().x, cardHexagon.getDimension().y);
        batch.draw(starTexture, cardStar.getPosition().x, cardStar.getPosition().y, cardStar.getDimension().x, cardStar.getDimension().y);
        batch.draw(triangleTexture, cardTriangle.getPosition().x, cardTriangle.getPosition().y, cardTriangle.getDimension().x, cardTriangle.getDimension().y);
    }

    /**
     * Draw static figures except the one which must be animated.
     *
     * @param figureId id of figure which is animated
     */
    private void drawTable(int figureId) {
        switch (figureId) {
            case 1:

                batch.draw(cubeTexture, cardCube.getPosition().x, cardCube.getPosition().y, cardCube.getDimension().x, cardCube.getDimension().y);
                batch.draw(hexagonTexture, cardHexagon.getPosition().x, cardHexagon.getPosition().y, cardHexagon.getDimension().x, cardHexagon.getDimension().y);
                batch.draw(starTexture, cardStar.getPosition().x, cardStar.getPosition().y, cardStar.getDimension().x, cardStar.getDimension().y);
                batch.draw(triangleTexture, cardTriangle.getPosition().x, cardTriangle.getPosition().y, cardTriangle.getDimension().x, cardTriangle.getDimension().y);
                break;
            case 2:

                batch.draw(circleTexture, cardCircle.getPosition().x, cardCircle.getPosition().y, cardCircle.getDimension().x, cardCircle.getDimension().y);
                batch.draw(hexagonTexture, cardHexagon.getPosition().x, cardHexagon.getPosition().y, cardHexagon.getDimension().x, cardHexagon.getDimension().y);
                batch.draw(starTexture, cardStar.getPosition().x, cardStar.getPosition().y, cardStar.getDimension().x, cardStar.getDimension().y);
                batch.draw(triangleTexture, cardTriangle.getPosition().x, cardTriangle.getPosition().y, cardTriangle.getDimension().x, cardTriangle.getDimension().y);
                break;
            case 3:

                batch.draw(circleTexture, cardCircle.getPosition().x, cardCircle.getPosition().y, cardCircle.getDimension().x, cardCircle.getDimension().y);
                batch.draw(cubeTexture, cardCube.getPosition().x, cardCube.getPosition().y, cardCube.getDimension().x, cardCube.getDimension().y);
                batch.draw(starTexture, cardStar.getPosition().x, cardStar.getPosition().y, cardStar.getDimension().x, cardStar.getDimension().y);
                batch.draw(triangleTexture, cardTriangle.getPosition().x, cardTriangle.getPosition().y, cardTriangle.getDimension().x, cardTriangle.getDimension().y);
                break;
            case 4:

                batch.draw(circleTexture, cardCircle.getPosition().x, cardCircle.getPosition().y, cardCircle.getDimension().x, cardCircle.getDimension().y);
                batch.draw(cubeTexture, cardCube.getPosition().x, cardCube.getPosition().y, cardCube.getDimension().x, cardCube.getDimension().y);
                batch.draw(hexagonTexture, cardHexagon.getPosition().x, cardHexagon.getPosition().y, cardHexagon.getDimension().x, cardHexagon.getDimension().y);
                batch.draw(triangleTexture, cardTriangle.getPosition().x, cardTriangle.getPosition().y, cardTriangle.getDimension().x, cardTriangle.getDimension().y);
                break;
            case 5:

                batch.draw(circleTexture, cardCircle.getPosition().x, cardCircle.getPosition().y, cardCircle.getDimension().x, cardCircle.getDimension().y);
                batch.draw(cubeTexture, cardCube.getPosition().x, cardCube.getPosition().y, cardCube.getDimension().x, cardCube.getDimension().y);
                batch.draw(hexagonTexture, cardHexagon.getPosition().x, cardHexagon.getPosition().y, cardHexagon.getDimension().x, cardHexagon.getDimension().y);
                batch.draw(starTexture, cardStar.getPosition().x, cardStar.getPosition().y, cardStar.getDimension().x, cardStar.getDimension().y);
                break;
        }
    }

    /**
     * Check if card is clicked by comparing the area which ocuppy card and the area where the click was made.
     *
     * @return id of selected card if there is one
     */
    private int clickPositionCard() {
        for (int i = 0; i < cards.size(); i++) {
            if (clickPosition.x >= cards.get(i).getPosition().x &&
                    clickPosition.x <= (cards.get(i).getPosition().x + cards.get(i).getDimension().x) &&
                    clickPosition.y >= cards.get(i).getPosition().y &&
                    clickPosition.y <= (cards.get(i).getPosition().y + cards.get(i).getDimension().y)) {

                return cards.get(i).getId();
            }
        }
        return 0;
    }

    /**
     * Restarts the index of selected figures.
     */
    private void restartFigureIndex() {
        if (figureIndex == array_of_figures.size() - 1) {
            figureIndex = 0;
        }
    }

    /**
     * Deppending of the figure id it corresponding sound is played.
     * @param figureId
     */
    private void playSound(int figureId) {
        if (figureId == 1) {
            triangleSound.play();
        } else if (figureId == 2) {
            cubeSound.play();
        } else if (figureId == 3) {
            hexagonSound.play();
        } else if (figureId == 4) {
            starSound.play();
        } else if (figureId == 5) {
            triangleSound.play();
        }
    }

    /**
     * Logic of whole game, here we reproduce animations and controll the figures click and che
     */
    private void game() {
        if (array_of_figures.isEmpty()) {
            array_of_figures.add(generateRandomColor());
            array_of_figures.add(generateRandomColor());
            array_of_figures.add(generateRandomColor());
        }

        if (figureIndex < array_of_figures.size()) {
            if (array_of_figures.get(figureIndex) == 1) {
                if (isPlayingSound == false) {
                    circleSound.play();
                    isPlayingSound = true;
                }
                currentFrame = (TextureRegion) circleAnimation.getKeyFrame(elapsedTime);
                drawTable(1);
                batch.draw(currentFrame, cardCircle.getPosition().x, cardCircle.getPosition().y, cardCircle.getDimension().x, cardCircle.getDimension().y);
                if (circleAnimation.isAnimationFinished(elapsedTime)) {
                    isPlayingSound = false;
                    figureIndex++;
                    //counter must be reset to do the next complete animation
                    elapsedTime = 0;
                }
            } else if (array_of_figures.get(figureIndex) == 2) {
                if (isPlayingSound == false) {
                    cubeSound.play();
                    isPlayingSound = true;
                }
                currentFrame = (TextureRegion) cubeAnimation.getKeyFrame(elapsedTime);
                drawTable(2);
                batch.draw(currentFrame, cardCube.getPosition().x, cardCube.getPosition().y, cardCube.getDimension().x, cardCube.getDimension().y);
                if (cubeAnimation.isAnimationFinished(elapsedTime)) {
                    isPlayingSound = false;
                    figureIndex++;
                    elapsedTime = 0;
                }
            } else if (array_of_figures.get(figureIndex) == 3) {
                if (isPlayingSound == false) {
                    hexagonSound.play();
                    isPlayingSound = true;
                }
                currentFrame = (TextureRegion) hexagonAnimation.getKeyFrame(elapsedTime);
                drawTable(3);
                batch.draw(currentFrame, cardHexagon.getPosition().x, cardHexagon.getPosition().y, cardHexagon.getDimension().x, cardHexagon.getDimension().y);
                if (hexagonAnimation.isAnimationFinished(elapsedTime)) {
                    isPlayingSound = false;
                    figureIndex++;
                    elapsedTime = 0;
                }
            } else if (array_of_figures.get(figureIndex) == 4) {
                if (isPlayingSound == false) {
                    starSound.play();
                    isPlayingSound = true;
                }
                currentFrame = (TextureRegion) starAnimation.getKeyFrame(elapsedTime);
                drawTable(4);
                batch.draw(currentFrame, cardStar.getPosition().x, cardStar.getPosition().y, cardStar.getDimension().x, cardStar.getDimension().y);
                if (starAnimation.isAnimationFinished(elapsedTime)) {
                    isPlayingSound = false;
                    figureIndex++;
                    //counter must be reset to do the next complete animation
                    elapsedTime = 0;
                }
            } else if (array_of_figures.get(figureIndex) == 5) {
                if (isPlayingSound == false) {
                    triangleSound.play();
                    isPlayingSound = true;
                }
                currentFrame = (TextureRegion) triangleAnimation.getKeyFrame(elapsedTime);
                drawTable(5);
                batch.draw(currentFrame, cardTriangle.getPosition().x, cardTriangle.getPosition().y, cardTriangle.getDimension().x, cardTriangle.getDimension().y);
                if (triangleAnimation.isAnimationFinished(elapsedTime)) {
                    isPlayingSound = false;
                    figureIndex++;
                    //counter must be reset to do the next complete animation
                    elapsedTime = 0;
                }
            }
        }
        else {
            drawCardsGrid();
            if (!fail) {
                if (Gdx.input.justTouched()) {
                    touchCounter++;
                    cameraUnprojectCoordinates = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
                    clickPosition.set(cameraUnprojectCoordinates.x, cameraUnprojectCoordinates.y);

                    if (clickPositionCard() == array_of_figures.get(touchCounter - 1)) {
                        playSound(clickPositionCard());
                        score = score + 5;
                        finalScore = score;
                        correctAnswers++;
                        if ((clickPositionCard() == array_of_figures.get(touchCounter - 1)) && (array_of_figures.size() == touchCounter)) {
                            //last card was the correct one and must generate another one
                            score = score + 5 * (array_of_figures.size());
                            array_of_figures.add(generateRandomColor());
                            restartFigureIndex();
                            touchCounter = 0;
                            elapsedTime = 0;
                            waitTime();
                        }
                    } else {
                        failSound.play();
                        fail = true;
                    }
                }
            } else {
                points.add(score);
                points.add(correctAnswers);
                dispose();
                gameOverScreen = new GameOverScreen(colors, points);
                hide();
                game.setScreen(gameOverScreen);
            }
        }
    }

    /**
     * Method used to enter a little delay between figure animations
     */
    public void waitTime() {
        double current = Gdx.app.getGraphics().getDeltaTime();
        while (current <500) {
            current = current + Gdx.app.getGraphics().getDeltaTime();
        }
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        assetManager.manager.dispose();
    }
}

