package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;

public class AnswerButton extends GameButton{

    private String number;

    public AnswerButton(int width, int height, Vector2 position) {
        super(width, height, position);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number){
        this.number = number;
    }

}
