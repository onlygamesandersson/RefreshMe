package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;
import com.hpscds.refreshme.Constants;

public class Card {


    private int id;
    private Vector2 position;
    private Vector2 dimension = new Vector2(Constants.getCardWidth(), Constants.getCardHeight());
    private boolean isTouched = false;


    public int getId() {
        return id;
    }

    public void setPosition(Vector2 position) {
        this.position = new Vector2(position);
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setDimension(Vector2 dimension) {
        this.dimension = dimension;
    }

    public Vector2 getDimension() {
        return dimension;
    }

    public void setIsTouched(boolean isTouched) {
        this.isTouched = isTouched;
    }

    public boolean getIsTouched() {
        return isTouched;
    }

    public Card(Vector2 position) {
        setPosition(position);
    }

}
