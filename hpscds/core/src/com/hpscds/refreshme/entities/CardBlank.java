package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;

public class CardBlank extends Card {

    /**
     * Class Attributes
     */
    private int id = 0;
    private String name = "blank";


    /**
     * Class Methods
     */
    public int getId() {
        return this.id;
    }

    public CardBlank(Vector2 position) {
        super(position);
    }
}
