package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;

public class CardCircle extends Card {

    /**
     * Class Attributes
     */
    private int id = 1;
    private String name = "circle";
    private boolean isTouched = false;

    /**
     * Class Methods
     */
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setIsTouched(boolean isTouched) {
        this.isTouched = isTouched;
    }

    public boolean getIsTouched() {
        return isTouched;
    }

    public CardCircle(Vector2 position) {
        super(position);
    }
}
