package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;

public class CardStar extends Card {

    /**
     * Class Attributes
     */
    private int id = 4;
    private String name = "star";
    private boolean isTouched = false;

    /**
     * Class Methods
     */
    public int getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public void setIsTouched(boolean isTouched) {
        this.isTouched = isTouched;
    }

    public boolean getIsTouched() {
        return isTouched;
    }

    public CardStar(Vector2 position) {
        super(position);
    }
}
