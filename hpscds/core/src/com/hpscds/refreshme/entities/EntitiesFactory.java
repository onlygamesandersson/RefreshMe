package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;

public class EntitiesFactory {

    public EntitiesFactory() {
    }

    public Card generateEntity(int id, Vector2 position) {
        if (id == 0) {
            return createBlank(position);
        } else if (id == 1) {
            return createCircle(position);
        } else if (id == 2) {
            return createCube(position);
        } else if (id == 3) {
            return createHexagon(position);
        } else if (id == 4) {
            return createStar(position);
        } else if (id == 5) {
            return createTriangle(position);
        }
        return null;
    }

    public CardBlank createBlank(Vector2 position) {
        return new CardBlank(position);
    }

    public CardCircle createCircle(Vector2 position) {
        return new CardCircle(position);
    }

    public CardCube createCube(Vector2 position) {
        return new CardCube(position);
    }

    public CardHexagon createHexagon(Vector2 position) {
        return new CardHexagon(position);
    }

    public CardStar createStar(Vector2 position) {
        return new CardStar(position);
    }

    public CardTriangle createTriangle(Vector2 position) {
        return new CardTriangle(position);
    }
}
