package com.hpscds.refreshme.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.hpscds.refreshme.api.GameController;
import com.hpscds.refreshme.maths.Maths;

public class AbstractScreen implements Screen {

    protected Game game;

    public AbstractScreen(Game game){ //Gamecontroller ?
        this.game = game;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
